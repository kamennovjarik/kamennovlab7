void func1(int a){			//init
	int c = 0;				//commit9
	print(a-2);				//commit9
	print(a-c+2);			//commit10
	print(c+a);				//commit4
}							//init

int func2(int a){			//init
	print(a*2 - 1);			//commit7
	return a*2;				//init
}							//init

int func3(int a){			//init
	print(a+1)				//commit2
	print(a)				//commit7
	return a;				//commit3
}							//init

int func4(int a, int b){	//init
	int c = a-b++;			//commit10
	bool d = a&&b;			//commit10
	return a+b;				//init
}							//init

int func5(int a, int b){	//init
	print(a*b)				//commit8
	return a*b;				//init
}							//init

int func6(int a, int b){	//init
	print(a%b)				//commit9
	print(a);				//commit6
	return a/b;				//init
}							//init

int func7(int a, int b){	//init
	print(a-b+2);			//commit10
	return a-b+2;			//commit8
}							//init

int func8(int a, int b){	//init
	print(a+b);				//commit6
	print(a*3);				//commit9
	return a+b;				//commit3
}							//init
